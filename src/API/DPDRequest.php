<?php

namespace d3x\DPD\API;

use d3x\DPD\API\Exceptions\CurlException;
use d3x\DPD\API\Exceptions\DPDException;
use Illuminate\Support\Facades\Config;

abstract class DPDRequest
{
    public $action;
    public $country;
    public $method;
    public $response;

    protected $url;
    protected $username;
    protected $password;
    protected $params;

    public function __construct()
    {
        $this->url = Config::get('dpd.url');
        $this->username = Config::get('dpd.username');
        $this->password = Config::get('dpd.password');
        $this->country = Config::get('dpd.default_country_code');
        $this->params = [
            "username" => $this->username,
            "password" => $this->password,
        ];
    }

    public function getOptions()
    {
        $options = [
            CURLOPT_URL => $this->url . $this->action,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $this->method,
        ];
        if ($this->params) {
            $query_string = $this->encodedParams();
            if (!empty($query_string))
                $options[CURLOPT_URL] .= '?' . $query_string;
        }
        return $options;
    }

    public function call($raw = false)
    {
        $curl = curl_init();
        $options = $this->getOptions();

        curl_setopt_array($curl, $options);
        $response = curl_exec($curl);
        if ($raw)
            return $response;
        if (curl_errno($curl)) {
            throw new CurlException($curl);
        } else {
            $http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            $options["response_code"] = $http_code;
            if (!in_array($http_code, [200, 201, 202, 204, 206]))
                throw new DPDException($options, $response);
        }
        curl_close($curl);
        $this->response = json_decode($response);
    }

    public function addParams($params)
    {
        $this->params = array_merge($this->params, $params);
    }

    public function encodedParams()
    {
        return http_build_query($this->params);
    }
}
