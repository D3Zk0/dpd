<?php

namespace d3x\DPD\API\Exceptions;

use Illuminate\Support\Facades\Log;

class DPDException extends \Exception
{
    public function __construct($options, $response)
    {
        // Pridobite curl možnosti
        $options['response'] = $response;
        // Zapišite curl možnosti v log
        Log::error("Napaka pri DPD zahtevku", $options);
        dd($response);
        // Pokličite konstruktor nadrazreda
        parent::__construct("Napaka pri DPD zahtevku.");
    }
}
