<?php

namespace d3x\DPD\API\Exceptions;

use Illuminate\Support\Facades\Log;

class CurlException extends \Exception
{
    public function __construct($curl)
    {
        // Pridobite curl možnosti
        $options = curl_getinfo($curl);

        // Formatirajte curl možnosti za izpis
        $optionsForLog = print_r($options, true);

        // Zapišite curl možnosti v log
        Log::error("Napaka pri curl zahtevku, možnosti:\n" . $optionsForLog, );
        dd($options, $optionsForLog);
        // Pokličite konstruktor nadrazreda
        parent::__construct("Napaka pri curl zahtevku.");
    }
}
