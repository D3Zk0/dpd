<?php

namespace d3x\DPD\API;

class Parcel extends DPDRequest
{
    public function __construct($method)
    {
        $this->action = "/api/parcel";
        $this->method = $method;
        parent::__construct();
    }

    public static function create($params)
    {
        $client = (new self("POST"));
        $client->addParams($params);
        $client->action = "/api/parcel/parcel_import";
        $client->call();
        return collect($client->response);
    }


    public static function label($params)
    {
        $client = (new self("POST"));
        $client->addParams($params);
        $client->action = "/api/parcel/parcel_print";
        return $client->call(true);
    }
}
