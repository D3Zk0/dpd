<?php

namespace d3x\DPD;

use Illuminate\Support\ServiceProvider;
use d3x\starter\StarterServiceProvider;

class DPDServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/config/dpd.php' => config_path('dpd.php'),
        ]);
    }

    public function register()
    {
        $this->app->register(StarterServiceProvider::class);
        $this->mergeConfigFrom(
            __DIR__ . '/config/dpd.php', 'dpd'
        );
    }
}
