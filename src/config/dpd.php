<?php

return [
    "url" => env("DPD_URL", ""),
    "username" => env("DPD_USERNAME", ""),
    "password" => env("DPD_PASSWORD", ""),
    "default_country_code" => env("DPD_COUNTRY_CODE"),
];
